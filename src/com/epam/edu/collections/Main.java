package com.epam.edu.collections;

import org.apache.log4j.BasicConfigurator;

import com.epam.edu.collections.util.CollectionsPerformanceUtils;

public class Main {

	private static final String COLLECTIONS_TO_INVESTIGATE = "collections.txt";

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		BasicConfigurator.configure();
		CollectionsPerformanceUtils.performanceMeasure(COLLECTIONS_TO_INVESTIGATE);
	}
}
