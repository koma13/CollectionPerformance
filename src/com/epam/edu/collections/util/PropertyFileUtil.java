package com.epam.edu.collections.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PropertyFileUtil {

	public static List<String> readFromPropertyFile(String fileNameWithCollections) {
		BufferedReader br = null;
		List<String> collectionsToInvestigate = new ArrayList<String>(); 
		try {
			String specificCollection;
			br = new BufferedReader(new FileReader(fileNameWithCollections));
			while ((specificCollection = br.readLine()) != null) {
				collectionsToInvestigate.add(specificCollection);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return collectionsToInvestigate;
	}
}
