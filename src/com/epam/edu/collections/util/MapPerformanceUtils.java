package com.epam.edu.collections.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class MapPerformanceUtils {

	private static final double TO_MILLISECONDS = 1000000.0;
	private static int COLLECTION_SIZE = 100;
	static Logger logger = Logger.getLogger(MapPerformanceUtils.class.getName());

	public static void mapMeasuring(String sCurrentLine) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Runtime.getRuntime().gc();
		long startUsedMemory = CollectionsPerformanceUtils.getMemoryIsUsed();
		long startTime = System.nanoTime();
		Map<Integer, String> map = extracted(sCurrentLine);
		for (int index = 0; index < COLLECTION_SIZE; index++) {
			map.put(index, "index");
		}
		double elapsedSeconds = System.nanoTime() - startTime;
		long usedMemory = CollectionsPerformanceUtils.getMemoryIsUsed() - startUsedMemory;
		logger.info("Time and memory to create list with 100 objects of " + map.getClass() + " is " + CollectionsPerformanceUtils.roundOff(elapsedSeconds / TO_MILLISECONDS, 2) + " milliseconds and "
				+ usedMemory + " bytes");
		startTime = System.nanoTime();
		for (Iterator<?> entries = map.entrySet().iterator(); entries.hasNext();) {
			Entry<Integer, String> entry = (Entry) entries.next();
			entries.remove();
		}
		elapsedSeconds = System.nanoTime() - startTime;
		usedMemory = CollectionsPerformanceUtils.getMemoryIsUsed() - usedMemory;
		logger.info("Time and memory after deleting list with 100 objects of " + map.getClass() + " is " + CollectionsPerformanceUtils.roundOff(elapsedSeconds / TO_MILLISECONDS, 2)
				+ " milliseconds and " + usedMemory + " bytes");
	}

	private static Map<Integer, String> extracted(String concreteMap) {
		try {
			return ((Map<Integer, String>) Class.forName(concreteMap).newInstance());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
