package com.epam.edu.collections.util;

import java.util.List;

public class CollectionsPerformanceUtils {

	public static void performanceMeasure(String collectionsTxT) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		List <String> collectionsToInvestigate = PropertyFileUtil.readFromPropertyFile(collectionsTxT);
		for (String specificCollection : collectionsToInvestigate) {
			if (specificCollection.contains("Map"))
				MapPerformanceUtils.mapMeasuring(specificCollection);
			else {
				CollectionPerformanceUtils.measureTimeMemoryConsumption(specificCollection);
			}
		}

	}

	protected static double roundOff(double x, int position) {
		double temp = Math.pow(10.0, position);
		x *= temp;
		x = Math.round(x);
		return x / temp;
	}

	protected static long getMemoryIsUsed() {
		Runtime.getRuntime().gc();
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}
}
