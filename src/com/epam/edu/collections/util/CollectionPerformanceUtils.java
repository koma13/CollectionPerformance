package com.epam.edu.collections.util;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

public class CollectionPerformanceUtils {

	private static final double TO_MILLISECONDS = 1000000.0;
	private static int SIZE = 100;
	static Logger logger = Logger.getLogger(CollectionPerformanceUtils.class.getName());

	public static void measureTimeMemoryConsumption(String specificCollection) {
		Runtime.getRuntime().gc();
		long usedMemoryStart = getMemoryIsUsed();
		long startTime = System.nanoTime();

		Collection<Integer> collection = extractCollection(specificCollection);
		for (int index = 0; index <= SIZE; index++) {
			collection.add(index * 1000);
		}
		long endTime = System.nanoTime();
		long elapsedSeconds = endTime - startTime;

		long usedMemoryEnd = getMemoryIsUsed();
		long usedMemory = usedMemoryEnd - usedMemoryStart;

		logger.info("Time to create list with 100 objects of " + collection.getClass() + " is " + CollectionsPerformanceUtils.roundOff(elapsedSeconds / TO_MILLISECONDS, 2) + " milliseconds");
		logger.info("Memory to create  100 instances of " + collection.getClass() + " is " + usedMemory + " bytes");
		Runtime.getRuntime().gc();
		startTime = System.nanoTime();
		Iterator<Integer> iterator = collection.iterator();
		while (iterator.hasNext()) {
			iterator.next();
			iterator.remove();
		}
		endTime = System.nanoTime();
		elapsedSeconds = endTime - startTime;
		usedMemoryStart = usedMemoryStart - getMemoryIsUsed();
		logger.info("Time to delete list with 100 objects of " + collection.getClass() + " is " + CollectionsPerformanceUtils.roundOff(elapsedSeconds / TO_MILLISECONDS, 2) + " milliseconds");
		logger.info("Memory after deleting 100 objects of " + collection.getClass() + " is " + usedMemory + " bytes");
	}

	private static Collection<Integer> extractCollection(String specificCollection) {
		try {
			return (Collection<Integer>) Class.forName(specificCollection).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static long getMemoryIsUsed() {
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}

}